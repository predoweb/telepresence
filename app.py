import falcon
import Queue

class Movement(object):
	__bots = dict()

	def on_get(self, req, resp, bot_id):
		if bot_id not in self.__bots:
			resp.status = falcon.HTTP_404
			return False

		if self.__bots[bot_id].empty():
			resp.status = falcon.HTTP_204
			return False
			
		resp.body = self.__bots[bot_id].get()
		resp.status = falcon.HTTP_200

	def on_post(self, req, resp, bot_id, mov):
		if bot_id not in self.__bots:
			self.__bots[bot_id] = Queue.Queue()


		self.__bots[bot_id].put(mov)
		resp.status = falcon.HTTP_204

	def on_delete(self, req, resp, bot_id):
		if bot_id not in self.__bots:
			resp.status = falcon.HTTP_404
			return False

		del self.__bots[bot_id]
		resp.status = falcon.HTTP_204

api = falcon.API()
move = Movement()

api.add_route('/{bot_id}', move)
api.add_route('/{bot_id}/mov', move)
api.add_route('/{bot_id}/mov/{mov}', move)